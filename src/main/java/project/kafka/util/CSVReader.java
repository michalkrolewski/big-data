package project.kafka.util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CSVReader {

    public List<JSONObject> readData(String fileName) {
        try (BufferedReader csvReader = new BufferedReader(new FileReader(fileName))) {
            String[] headers = csvReader.readLine().split(",");
            return mapCSVToObjects(csvReader, headers);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private List<JSONObject> mapCSVToObjects(BufferedReader csvReader, String[] headers) {
        return csvReader.lines()
                .map(line -> line.split(","))
                .map(lines -> mapToJson(headers, lines))
                .collect(Collectors.toList());
    }

    private JSONObject mapToJson(String[] headers, String[] values) {
        JSONObject object = new JSONObject();
        for (int i = 0; i < headers.length; i++) {
            if (isNumeric(values[i])) {
                object.put(headers[i], Double.parseDouble(values[i]));
            } else {
                object.put(headers[i], values[i]);
            }
        }
        return object;
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
