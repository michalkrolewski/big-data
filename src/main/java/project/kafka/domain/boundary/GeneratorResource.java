package project.kafka.domain.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.kafka.domain.control.GeneratorService;
import project.kafka.domain.control.GeneratorStatus;
import project.kafka.domain.control.StatusCache;
import project.kafka.domain.entity.GeneratorEntity;

import java.util.Optional;

@RestController
@RequestMapping("/generator")
public class GeneratorResource {

    public static final String NAME_ATTRIBUTE = "name";
    public static final String NAME_PATH = "/{" + NAME_ATTRIBUTE + "}";
    public static final String STOP_PATH = "/stop";
    public static final String REMOVE_PATH = "/remove";

    StatusCache cache = StatusCache.getINSTANCE();

    @Autowired
    GeneratorService generatorService;

    @PostMapping()
    public GeneratorStatus runGenerator(@RequestBody GeneratorEntity dto) {
        generatorService.runGenerator(dto);
        return prepareStatus(dto.name);
    }

    @DeleteMapping(STOP_PATH + NAME_PATH)
    public void stopGenerator(@PathVariable(NAME_ATTRIBUTE) String name) {
        generatorService.stopGenerator(name);
    }

    @DeleteMapping(REMOVE_PATH + NAME_PATH)
    public void removeGenerator(@PathVariable(NAME_ATTRIBUTE) String name) {
        generatorService.removeGenerator(name);
    }

    @GetMapping(NAME_PATH)
    public GeneratorStatus getGeneratorStatus(@PathVariable(NAME_ATTRIBUTE) String name) {
        return prepareStatus(name);
    }

    private GeneratorStatus prepareStatus(String name) {
        Optional<GeneratorStatus> generatorStatus = cache.get(name);
        return generatorStatus.orElse(null);
    }
}
