package project.kafka.domain.boundary;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.kafka.domain.control.KafkaRepository;
import project.kafka.domain.entity.KafkaObject;
import project.kafka.util.KafkaAdminService;
import project.kafka.util.KafkaPublisherService;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/kafka")
public class KafkaResource {

    public static final String TOPIC_ATTRIBUTE = "topicName";

    public static final String TOPIC_PATH = "/topic/{" + TOPIC_ATTRIBUTE + "}";
    public static final String EVENT_PATH = "/event";

    @Autowired
    KafkaRepository repository;


    @PostMapping(EVENT_PATH)
    public void sendOnTopic(@RequestParam(TOPIC_ATTRIBUTE) String topicName, @RequestBody KafkaObject object) {
        Objects.requireNonNull(topicName);

        KafkaPublisherService publisher = KafkaPublisherService.getINSTANCE();
        publisher.sendOnTopic(topicName, object);
    }

    @GetMapping(value = EVENT_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getFromTopic(@RequestParam(TOPIC_ATTRIBUTE) String topicName) {
        Objects.requireNonNull(topicName);

        Collection<Map<String, Object>> objects = repository.getObjectsFromTopic(topicName);
        return new ResponseEntity<>(objects, HttpStatus.OK);
    }

    @GetMapping(TOPIC_PATH)
    public Set<String> getTopics() {
        KafkaAdminService adminService = KafkaAdminService.getINSTANCE();
        return adminService.getTopics();
    }

    @PostMapping(TOPIC_PATH)
    public void createTopic(@PathVariable(TOPIC_ATTRIBUTE) String topicName) {
        Objects.requireNonNull(topicName);

        KafkaAdminService adminService = KafkaAdminService.getINSTANCE();
        adminService.createTopic(topicName);
    }

    @DeleteMapping(TOPIC_PATH)
    public void deleteTopic(@PathVariable(TOPIC_ATTRIBUTE) String topicName) {
        Objects.requireNonNull(topicName);

        KafkaAdminService adminService = KafkaAdminService.getINSTANCE();
        adminService.deleteTopic(topicName);
    }

}
