package project.kafka.domain.control;

import project.kafka.domain.entity.GeneratorEntity;

public class GeneratorStatus {

    private Status status;
    private String name;
    private long delay;
    private String dataPath;
    private String topic;
    private Long counter;

    public GeneratorStatus(GeneratorEntity entity) {
        this.name = entity.name;
        this.topic = entity.topic;
        this.delay = entity.delay;
        this.dataPath = entity.path;
        this.status = Status.ACTIVE;
        this.counter = 0L;
    }

    public void setInactive() {
        status = Status.INACTIVE;
    }

    public void incrementCounter() {
        counter++;
    }

    public Status getStatus() {
        return status;
    }

    public String getDataPath() {
        return dataPath;
    }

    public long getDelay() {
        return delay;
    }

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public Long getCounter() {
        return counter;
    }

    enum Status {
        ACTIVE, INACTIVE
    }
}
