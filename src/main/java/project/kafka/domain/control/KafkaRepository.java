package project.kafka.domain.control;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import project.kafka.util.KafkaConsumerService;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class KafkaRepository {

    public Collection<Map<String, Object>> getObjectsFromTopic(String topicName) {
        KafkaConsumerService consumerService = KafkaConsumerService.getINSTANCE();
        ConsumerRecords<String, String> consumerRecords = consumerService.runConsumer(topicName);
        return StreamSupport.stream(consumerRecords.spliterator(), false)
                .map(ConsumerRecord::value)
                .map(JSONObject::new)
                .map(JSONObject::toMap)
                .collect(Collectors.toList());
    }
}