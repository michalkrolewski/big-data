package project.kafka.domain.control;

import project.kafka.domain.entity.GeneratorEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class StatusCache {

    private static final StatusCache INSTANCE = new StatusCache();
    private final Map<String, GeneratorStatus> statuses = new HashMap<>();

    private StatusCache() {
    }

    public static StatusCache getINSTANCE() {
        return INSTANCE;
    }

    public GeneratorStatus addOrGetExisting(GeneratorEntity entity) {
        if (statuses.containsKey(entity.name)) {
            return statuses.get(entity.name);
        }
        GeneratorStatus generatorStatus = new GeneratorStatus(entity);
        statuses.put(entity.name, generatorStatus);
        return generatorStatus;
    }

    public Optional<GeneratorStatus> get(String name) {
        if (statuses.containsKey(name)) {
            return Optional.of(statuses.get(name));
        }
        return Optional.empty();
    }

    public void remove(String name) {
        statuses.remove(name);
    }

}
