package project.kafka.domain.control;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class TimerCache {

    private static final TimerCache INSTANCE = new TimerCache();
    private final Map<String, Timer> timers = new HashMap<>();

    private TimerCache() {
    }

    public static TimerCache getINSTANCE() {
        return INSTANCE;
    }

    public Timer addOrGetExisting(String name) {
        if (!timers.containsKey(name)) {
            Timer timer = new Timer();
            timers.put(name, timer);
            return timer;
        }
        return timers.get(name);
    }

    public void remove(String name) {
        if (timers.containsKey(name)) {
            Timer timer = timers.get(name);
            timer.cancel();
            timers.remove(name);
        }
    }
}
