package project.kafka.domain.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import project.kafka.domain.entity.GeneratorEntity;
import project.kafka.domain.entity.Task;
import java.util.Optional;
import java.util.Timer;

@Service
public class GeneratorService {

    StatusCache statusCache = StatusCache.getINSTANCE();
    TimerCache timerCache = TimerCache.getINSTANCE();

    @Autowired
    KafkaRepository repository;

    @Async
    public void runGenerator(GeneratorEntity entity) {
        GeneratorStatus generatorStatus = statusCache.addOrGetExisting(entity);
        createIntervalTimer(generatorStatus);
    }

    public void createIntervalTimer(GeneratorStatus status) {
        Timer timer = timerCache.addOrGetExisting(status.getName());
        timer.scheduleAtFixedRate(new Task(status), status.getDelay(), status.getDelay());
    }

    public void stopGenerator(String name) {
        Optional<GeneratorStatus> generatorStatus = statusCache.get(name);
        if (generatorStatus.isPresent()) {
            timerCache.remove(name);
            GeneratorStatus status = generatorStatus.get();
            status.setInactive();
        }
    }

    public void removeGenerator(String name) {
        timerCache.remove(name);
        statusCache.remove(name);
    }
}
