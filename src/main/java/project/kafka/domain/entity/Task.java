package project.kafka.domain.entity;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.kafka.domain.control.GeneratorService;
import project.kafka.domain.control.GeneratorStatus;
import project.kafka.util.CSVReader;
import project.kafka.util.KafkaPublisherService;

import java.util.List;
import java.util.TimerTask;

public class Task extends TimerTask {

    private static final String msg = "Generator : %s sent object on kafka topic: %s.";

    private static final Logger logger = LoggerFactory.getLogger(GeneratorService.class);
    private KafkaPublisherService publisher = KafkaPublisherService.getINSTANCE();

    private GeneratorStatus status;
    private List<JSONObject> objects;

    public Task(GeneratorStatus status) {
        this.status = status;
        this.objects = new CSVReader().readData(status.getDataPath());
    }

    public void run() {
        if (status.getCounter() < objects.size()) {
            JSONObject object = objects.get(status.getCounter().intValue());
            publisher.sendOnTopic(status.getTopic(), object);
            status.incrementCounter();
            logger.info(String.format(msg, status.getName(), status.getTopic()));
        } else {
            status.setInactive();
        }
    }
}